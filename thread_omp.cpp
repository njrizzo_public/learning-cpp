#include <iostream>
#include <unistd.h>	// To include sleep function
#include <ctime>		// To get system time
#include <thread>
#include <omp.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>

using namespace std;
using namespace cv;

Mat image1;
Mat image2;
Mat result;

int main(void);

int main()
{
int  f,e, i, c, r, w;

	image1 = imread("/home2/rizzo/Photos/joao/NDF_3849.JPG");
	image2 = imread("/home2/rizzo/Photos/joao/NDF_3850.JPG");
	result = image1.clone();
	if ( image1.empty() ){
		cout << "Image 1 not exist" << endl;
		exit(-1);
	}
	if ( image2.empty() ){
		cout << "image 2 not exist" << endl;
		exit(-2);
	}
	f = 0;
	r = image1.rows;
	w = image1.cols;

/// #pragma omp parallel for reduction(+:pixel1, pixel2)
//for(int g=0; g < 100; g++)
#pragma omp parallel private (i,c)
{
Vec3b	pixel1, pixel2;
	for( i=0; i < r; i++)
		for (c=0; c< w ; c++){
			pixel1 = image1.at<Vec3b>(i,c); 
			pixel2 = image2.at<Vec3b>(i,c);
			pixel1 += pixel2;
			result.at<Vec3b>(i,c) = pixel1;
		}
}
cout << "Imagem 1 = " << image1.at<Vec3b>(image1.rows -1 ,image1.cols-1) << endl;
cout << "Imagem 2 = " << image2.at<Vec3b>(image1.rows -1 ,image1.cols-1) << endl;
cout << "Imagem 3 = " << result.at<Vec3b>(image1.rows -1 ,image1.cols-1) << endl;
return 0;
}
