#include <iostream>
#include <unistd.h>   // To include sleep function
#include <ctime>   // To get system time
#include <thread>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>


using namespace std;
using namespace cv;

Mat image1;
Mat image2;
Mat result;

void func(  int &row);


void func(  int &row)
{
	int r,w;
	Vec3b		pixel1, pixel2;
	//r = *(reinterpret_cast<unsigned int*>(row));
	r = row;
	w =  image1.cols;
	for (int c=0; c< w ; c++){
		pixel1 = image1.at<Vec3b>(r,c); 
		pixel2 = image2.at<Vec3b>(r,c);
		pixel1 += pixel2;
		result.at<Vec3b>(r,c) = pixel1;
	}
}

// main function
int main()
{
int  f,e;
int r,c, w;

thread	t[5000];

	image1 = imread("/home2/rizzo/Photos/joao/NDF_3849.JPG");
	image2 = imread("/home2/rizzo/Photos/joao/NDF_3850.JPG");
	result = image1.clone();
	if ( image1.empty() ){
		cout << "Image 1 not exist" << endl;
		exit(-1);
	}
	if ( image2.empty() ){
		cout << "image 2 not exist" << endl;
		exit(-2);
	}
   // Step 1: Declaring thread
	f = 0;
	//int hc = thread::hardware_concurrency();
	r = image1.rows;
	w = image1.cols;
   for(unsigned int i=0; i<r; i++)
   {
		cout << "Creating thread [ " << i << " ]" << flush;
      t[i] = thread(func, &i); 
//      if ( e )
//			cout <<"Error to create a thread [ " << i << " ]" << endl;
//		else
//		{
			// Step 3: main thread doesn't waits for child thread to complete
			t[i].detach(); 
			cout << "Detached" << endl;
//		}
	}

	for( int i=0; i < r; i++)
		t[i].join();

cout << "Imagem 1 = " << image1.at<Vec3b>(100,100) << endl;
cout << "Imagem 2 = " << image2.at<Vec3b>(100,100) << endl;
cout << "Imagem 3 = " << result.at<Vec3b>(100,100) << endl;
//	cout << "Flags " << f << "\r" << flush;
cout << endl;
// Exiting after completion
exit(EXIT_SUCCESS); 
return 0;
}
