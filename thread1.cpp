#include <iostream>
#include <unistd.h>	// To include sleep function
#include <ctime>		// To get system time
#include <thread>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>


using namespace std;
using namespace cv;

Mat image1;
Mat image2;
Mat result;

volatile unsigned int resultado[10];

int main(void);
void *func( void *arg);
void *foo( void *arg );

void *foo( void *arg )
{
	unsigned int i;
	i = *(reinterpret_cast<unsigned int*>(arg));

	resultado[i] = i * 10;
	sleep( 5 );
}

void *func( void *row)
{
	volatile unsigned int r,w;
	void *p;
	Vec3b		pixel1, pixel2;
	p = NULL;
	r = *(reinterpret_cast<unsigned int*>(row));
	w =  image1.cols;
	for (int c=0; c< w ; c++){
		pixel1 = image1.at<Vec3b>(r,c); 
		pixel2 = image2.at<Vec3b>(r,c);
		pixel1 += pixel2;
		result.at<Vec3b>(r,c) = pixel1;
	}
	return p;
}

int main()
{
int  f,e;
unsigned int r,c, w;

pthread_t 	thread[5000];

	image1 = imread("/home2/rizzo/Photos/joao/NDF_3849.JPG");
	image2 = imread("/home2/rizzo/Photos/joao/NDF_3850.JPG");
	result = image1.clone();
	if ( image1.empty() ){
		cout << "Image 1 not exist" << endl;
		exit(-1);
	}
	if ( image2.empty() ){
		cout << "image 2 not exist" << endl;
		exit(-2);
	}
	f = 0;
	r = image1.rows;
	w = image1.cols;
	r = 10;
   for(unsigned int i=0; i<r; i++)
   {
		cout << "Creating thread [ " << i << " ]" << endl;
      e = pthread_create( &thread[i], 0, &foo, &i); 
      if ( e )
			cout <<"Error to create a thread [ " << i << " ]" << endl;
		else
		{
			cout << "Created ... " << flush;
			pthread_detach( thread[i]); 
			cout << "Deatached" << endl;
		}
	}

	for( int i=0; i < r; i++)
		pthread_join(thread[i], NULL);
	
	for (unsigned int i = 0; i < r; i++)
		cout << "Reultado[" << i << "] = " << resultado[i] << endl;

/*
cout << "Imagem 1 = " << image1.at<Vec3b>(image1.rows -1 ,image1.cols-1) << endl;
cout << "Imagem 2 = " << image2.at<Vec3b>(image1.rows -1 ,image1.cols-1) << endl;
cout << "Imagem 3 = " << result.at<Vec3b>(image1.rows -1 ,image1.cols-1) << endl;
*/

return 0;
}
