WHERE		!= uname -m
HOST		!= hostname

ALLOPENCVLIBS=	opencv_calib3d opencv_core opencv_features2d		\
					opencv_flann opencv_highgui opencv_imgcodecs		\
					opencv_imgproc opencv_ml opencv_objdetect 		\
					opencv_photo opencv_shape opencv_stitching 		\
					opencv_superres opencv_video opencv_videoio 		\
					opencv_videostab											

OPENCV_LIBS+= -lopencv_core -lopencv_highgui -lopencv_imgproc 	\
					-lopencv_imgcodecs -lopencv_video -lopencv_videoio -lopencv_objdetect
#SQLITE_LIBS+= -lsqlite3
OPENMP+=-lomp
THREADS=-lpthread


# Default program name 
#PROG_NAME= image
#Default path to bin, include and lib
DEFAULT_LIB_INSTALL_PATH= ../lib
DEFAULT_BIN_INSTALL_PATH= ../bin
LOCAL_INCLUDE_DIR= -I .  -I ../include
# Use variables set to default
# Change if need
INSTALL_BIN= $(DEFAULT_BIN_INSTALL_PATH)
INSTALL_LIB= $(DEFAULT_LIB_INSTALL_PATH)

.if ( ${WHERE} == "arm64" )
CPPFLAGS	+=-Oz -mfix-cortex-a53-835769 
.endif
DEBUGFLAG+= -Wall				# until under devolopment, show all warnnings and debug features
CPPFLAGS += --std=c++17 $(DEBUGFLAG) 
CFLAGS	+= $(DEBUGFLAG) 
.if ( ${HOST} == "lotus" )
CPPFLAG += -lstdc++fs
.endif
INCLUDE_DIR+=	-I/usr/local/include -I/usr/include \
					$(LOCAL_INCLUDE_DIR)
LIBS_DIR += -L/usr/lib -L. -L/usr/local/lib -L$(INSTALL_LIB)
LIBS += $(OPENCV_LIBS) $(OPENMP) -lm
#
# All source Libs files name
#
SRC	= 	nothread.cpp		\
			thread_omp.cpp		\
			thread1.cpp			\
			thread2.cpp			\
			no_init_vector.cpp\
			init_vector.cpp
#
# All Object Libs files
#
OBJ	=	nothread.o			\
			thread_omp.o		\
			thread1.o			\
			thread2.o			\
			init_vector.o		\
			no_init_vector.o



#
# All binaries files
#
BINS = 	nothread		\
			thread_omp	\
			thread1		\
			thread1		\
			init_vector	\
			no_init_vector


.cpp.o:

	clang++ -o $@ -c $< $(INCLUDE_DIR) $(CPPFLAGS)

all: $(BINS)


thread1.o: thread1.cpp $(INCLUDE_DEP)


thread2.o: thread2.cpp $(INCLUDE_DEP)

	
nothread.o: nothread.cpp $(INCLUDE_DEP)


thread_omp.o: thread_omp.cpp $(INCLUDE_DEP)


init_vector.o: init_vector.cpp $(INCLUDE_DEP)


no_init_vector.o: no_init_vector.cpp $(INCLUDE_DEP)




thread1: thread1.o 

	clang++ -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR)	$(INCLUDE_LIB) \
							$(LIBS_DIR) $(LIBS) $(THREADS)
	
thread2: thread2.o 

	clang++ -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR)	$(INCLUDE_LIB) \
							$(LIBS_DIR) $(LIBS) 
	
nothread: nothread.o 

	clang++ -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR)	$(INCLUDE_LIB) \
							$(LIBS_DIR) $(LIBS) 
	
thread_omp: thread_omp.o 

	clang++ -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR)	$(INCLUDE_LIB) \
							$(LIBS_DIR) $(LIBS) 

init_vector: init_vector.o 

	clang++ -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR)	$(INCLUDE_LIB) \
							$(LIBS_DIR) $(LIBS) 
no_init_vector: no_init_vector.o 

	clang++ -o $@ $> $(CPPFLAGS)  $(INCLUDE_DIR)	$(INCLUDE_LIB) \
							$(LIBS_DIR) $(LIBS) 



clean:

	@echo -n Cleaning ... 
	@- rm -rf $(BINS) *.o *.core >> /dev/null
	@echo Done.


