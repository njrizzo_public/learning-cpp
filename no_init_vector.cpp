#include <iostream>
#include <unistd.h>	// To include sleep function
#include <omp.h>

#define MAX_VECTOR 	10000000
#define MAX_LOOP		1000

int main(void);

int main(void)
{
	int v[MAX_VECTOR];
	int loop;
	long long int c;

	for( loop=0; loop < MAX_LOOP; loop ++ )
		for( c=0;c < MAX_VECTOR; c++)
			v[c] = 10;
	
	return 0;
}

